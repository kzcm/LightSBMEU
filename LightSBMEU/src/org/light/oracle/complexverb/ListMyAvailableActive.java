package org.light.oracle.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.light.complexverb.TwoDomainVerb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.Type;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class ListMyAvailableActive extends TwoDomainVerb{

	@Override
	public Method generateDaoImplMethod() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodString() throws Exception {
		return null;
	}

	@Override
	public String generateDaoImplMethodStringWithSerial() throws Exception {
		return null;
	}

	@Override
	public Method generateDaoMethodDefinition() throws Exception {
		return null;
	}

	@Override
	public String generateDaoMethodDefinitionString() throws Exception {
		return null;
	}

	@Override
	public Method generateServiceMethodDefinition() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport(this.slave.getPackageToken()+"."+this.slave.getDomainSuffix()+"."+this.slave.getCapFirstDomainNameWithSuffix());
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.setThrowException(true);
		return method;
	}

	@Override
	public String generateServiceMethodDefinitionString() throws Exception {
		return generateServiceMethodDefinition().generateMethodDefinition();
	}

	@Override
	public Method generateServiceImplMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));		
		method.setReturnType(new Type("Set",this.slave, this.slave.getPackageToken()));
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id", this.master.getDomainId().getClassType()));
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport(this.slave.getPackageToken()+"."+this.slave.getDomainSuffix()+"."+this.slave.getCapFirstDomainNameWithSuffix());
		method.addAdditionalImport(this.master.getPackageToken()+"."+this.master.getDaoSuffix()+"."+this.master.getStandardName()+"Dao");
		method.addAdditionalImport(this.slave.getPackageToken()+"."+this.slave.getServiceSuffix()+"."+this.slave.getStandardName()+"Service");
		method.setThrowException(true);
		method.addMetaData("Override");
		
		List<Writeable> list = new ArrayList<Writeable>();
		ListMyActive listMy = new ListMyActive(this.master,this.slave);
		list.add(new Statement(1000L,2,"Set<"+this.slave.getCapFirstDomainNameWithSuffix()+"> fullset = new TreeSet<"+this.slave.getCapFirstDomainNameWithSuffix()+">();"));
		list.add(new Statement(2000L,2,"fullset.addAll("+this.slave.getLowerFirstDomainName()+"Service.listActive"+this.slave.getCapFirstPlural()+"());"));
		list.add(new Statement(3000L,2,"Set<"+this.slave.getCapFirstDomainNameWithSuffix()+"> set = "+StringUtil.lowerFirst(listMy.getVerbName())+"("+this.master.getLowerFirstDomainName()+"Id"+");"));
		list.add(new Statement(4000L,2,"fullset.removeAll(set);	"));	
		list.add(new Statement(5000L,2,"return fullset;"));
		method.setMethodStatementList(WriteableUtil.merge(list));
		return method;
	}

	@Override
	public String generateServiceImplMethodString() throws Exception {
		return generateServiceImplMethod().generateMethodString();
	}

	@Override
	public String generateServiceImplMethodStringWithSerial() throws Exception {
		Method m = this.generateServiceImplMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}

	@Override
	public Method generateControllerMethod() throws Exception {
		Method method = new Method();
		method.setStandardName(StringUtil.lowerFirst(this.getVerbName()));		
		method.setReturnType(new Type("Map<String,Object>"));
		method.setThrowException(true);
		method.addAdditionalImport("java.util.List");
		method.addAdditionalImport("java.util.Set");
		method.addAdditionalImport("java.util.TreeSet");
		method.addAdditionalImport(this.slave.getPackageToken()+"."+this.slave.getDomainSuffix()+"."+this.slave.getCapFirstDomainNameWithSuffix());
		method.addAdditionalImport(this.master.getPackageToken()+"."+this.master.getServiceSuffix()+"."+this.master.getStandardName()+"Service");
		method.addAdditionalImport(this.master.getPackageToken()+"."+this.master.getServiceimplSuffix()+"."+this.master.getStandardName()+"ServiceImpl");
		method.addSignature(new Signature(1,this.master.getLowerFirstDomainName()+"Id",this.master.getDomainId().getClassType(), this.slave.getPackageToken(),"RequestParam (required = false)"));
		method.addMetaData("RequestMapping(value = \"/"+StringUtil.lowerFirst(method.getStandardName())+"\", method = RequestMethod.POST)");

		List<Writeable> wlist = new ArrayList<Writeable>();
		wlist.add(new Statement(1000L,2,"Map<String,Object> result = new TreeMap<String,Object>();"));
		wlist.add(new Statement(2000L,2,"Set<"+this.slave.getCapFirstDomainNameWithSuffix()+"> set = new TreeSet<"+this.slave.getCapFirstDomainNameWithSuffix()+">();"));
		wlist.add(new Statement(3000L,2,"if ("+this.master.getLowerFirstDomainName()+"Id"+"!=null) set = service."+StringUtil.lowerFirst(this.getVerbName())+"("+this.master.getLowerFirstDomainName()+"Id"+");"));
		wlist.add(new Statement(4000L,2,"result.put(\"success\",true);"));
		wlist.add(new Statement(5000L,2,"result.put(\"rows\",set);"));
		wlist.add(new Statement(6000L,2,"return result;"));	
		method.setMethodStatementList(WriteableUtil.merge(wlist));
		
		return method;
	}

	@Override
	public String generateControllerMethodString() throws Exception {
		Method m = this.generateControllerMethod();
		return m.generateMethodString();
	}

	@Override
	public String generateControllerMethodStringWithSerial() throws Exception {
		Method m = this.generateControllerMethod();
		m.setContent(m.generateMethodContentStringWithSerial());
		m.setMethodStatementList(null);
		return m.generateMethodString();
	}
	
	public ListMyAvailableActive(Domain master,Domain slave){
		super();
		this.master = master;
		this.slave = slave;
		this.setVerbName("ListAvailableActive"+this.master.getCapFirstDomainName()+StringUtil.capFirst(this.slave.getAliasPlural())+"Using"+this.master.getCapFirstDomainName()+"Id");
		this.setLabel("列出可得");
	}
}