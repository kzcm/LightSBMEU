package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.utils.DomainUtil;
import org.light.utils.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.2.9
 *
 */
public class DeleteAllDataDomainsWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = -5368330749206424612L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteAllDataDomainsWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = -1;
		if (request.getParameter("serial")!=null) serial=Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
				
		try {
			Domain domain0 = new Domain();
			if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains()==null) {
				ExcelWizardFacade.getInstance().addDomain(domain0);
			}
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null && ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				domain0 = ExcelWizardFacade.getInstance().getDomains().get(serial);
			}
			ExcelWizardFacade.getInstance().setDataDomains(DomainUtil.deleteAllDataDomainsFromList(ExcelWizardFacade.getInstance().getDataDomains(),domain0.getStandardName()));
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
		}
	
	}
}
