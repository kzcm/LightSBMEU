package org.light.dao;

import org.light.domain.Class;
import org.light.domain.Naming;

public interface ClassDao {
	public Class generateClass(Naming naming, String standardName) throws Exception;
	public String generateClassString(Naming naming, String standardName) throws Exception;
}
