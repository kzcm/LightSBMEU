package org.light.utils;

import java.util.Collections;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.DragonHideStatement;
import org.light.domain.Statement;
import org.light.domain.StatementList;

public class WriteableUtil {
	public static StatementList merge(List<Writeable> cw){
		if (cw == null) return null;
		StatementList sc = new StatementList();
		Collections.sort(cw);
		long ii = 100;
		for (Writeable wc:cw){
			/*if (wc instanceof DragonHideStatement && ((DragonHideStatement)wc).isDragonHide()){
				Statement s = ((DragonHideStatement)wc).getNormalStatement();
				s.setSerial(ii);
				sc.add(s);
				ii += 100;
			}
			else*/
			if (wc instanceof Statement) {
				Statement s = (Statement)wc;
				s.setSerial(ii);
				sc.add(s);
				ii += 100;
			}
			else if (wc instanceof StatementList){
				List<Statement> list = ((StatementList) wc).getStatementList();
				Collections.sort(list);
				for (Statement s:list){
					s.setSerial(ii);
					ii += 100;
					sc.add(s);
				}
			}
		}
		return sc;
	}
}
